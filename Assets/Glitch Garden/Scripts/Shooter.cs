﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

    public GameObject projectile, gun;

    private Animator animator;
    private GameObject projectileParent;
    private Spawner myLaneSpawner;
	// Use this for initialization
	void Start () {
        
        animator = GameObject.FindObjectOfType<Animator>();
        projectileParent = GameObject.Find("Projectiles");
        if (!projectileParent)
        {
            projectileParent = new GameObject("Projectiles");
        }

        setMyLaneSpawner();
	}
	
	// Update is called once per frame
	void Update () {
        if (isAttackerAheadInLane())
        {
            animator.SetBool("isAttacking", true);
        }
        else
        {
            animator.SetBool("isAttacking", false);
        }
	}

    bool isAttackerAheadInLane()
    {
        if (myLaneSpawner.transform.childCount <= 0)
        {
            return false;
        }
        
        foreach(Transform attacker in myLaneSpawner.transform){
            if (attacker.transform.position.x > transform.position.x)
            {
                return true;
            }
           
        }

        return false;
    }

    void setMyLaneSpawner()
    {
        Spawner[] spawnerArray = GameObject.FindObjectsOfType<Spawner>();
        foreach(Spawner spawner in spawnerArray){
            if(spawner.transform.position.y == transform.position.y){
                myLaneSpawner = spawner;
                return;
            }
        }

        Debug.LogError(name + " cant find spawner in lane");
    }

    private void Fire()
    {
        GameObject newProjectile = Instantiate(projectile) as GameObject;
        newProjectile.transform.parent = projectileParent.transform;
        newProjectile.transform.position = gun.transform.position;
    }
}
