﻿using UnityEngine;
using System.Collections;

public class DefenderSpawner : MonoBehaviour {
    public Camera gameCamera;
    private GameObject defenderParent;
    private StarDisplay starDisplay;

    // Use this for initialization
    void Start()
    {
        starDisplay = GameObject.FindObjectOfType<StarDisplay>();

        defenderParent = GameObject.Find("Defenders");
        if (!defenderParent)
        {
            defenderParent = new GameObject("Defenders");
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        //print(Input.mousePosition);
        //print(CalculateWorldPointOfMouseClick());
        //print(SnapToGrid(CalculateWorldPointOfMouseClick()));
        GameObject defender = Button.selectedDefender;
        int defenderCost = defender.GetComponent<Defender>().starCost;
        if (Button.selectedDefender)
        {
            if (starDisplay.UseStars(defenderCost) == StarDisplay.Status.SUCCESS)
            {
                SpawnDefender(SnapToGrid(CalculateWorldPointOfMouseClick()), defender);
            }
            else
            {
                Debug.Log("Insufficient stars to spawn");
            }
        }

    }
    void SpawnDefender(Vector2 roundedPos, GameObject defender)
    {
        Quaternion zeroRot = Quaternion.identity;
        GameObject newDef = Instantiate(defender, roundedPos, zeroRot) as GameObject;
        newDef.transform.parent = defenderParent.transform;
    }
    Vector2 SnapToGrid(Vector2 rawWorldPos)
    {
        float newX = Mathf.RoundToInt(rawWorldPos.x);
        float newY = Mathf.RoundToInt(rawWorldPos.y);
        return new Vector2(newX, newY);
    }
    Vector2 CalculateWorldPointOfMouseClick()
    {
        float mouseX = Input.mousePosition.x;
        float mouseY = Input.mousePosition.y;
        float distanceFromCamera = 10f;

        Vector3 trip = new Vector3(mouseX, mouseY, distanceFromCamera);
        Vector2 worldPos = gameCamera.ScreenToWorldPoint(trip);
        return worldPos;
    }

}
