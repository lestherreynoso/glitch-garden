﻿using UnityEngine;
using System.Collections;

public class LoseCollider : MonoBehaviour {

	private LevelManager levelManager;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();
	}
	
	void OnTriggerEnter2D (Collider2D collider) {
        
        if (collider.GetComponent<Attacker>())
        {
            Debug.Log("Trigger Entered Game Lost by: " + collider);
            levelManager.LoadLevel("03b Lose");
        }
        else
        {
            Debug.Log("Game Lost by non Attacker: " + collider);
            return;
        }

	}
}
